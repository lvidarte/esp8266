#!/bin/bash

PORT=${1:-/dev/cu.SLAB_USBtoUART}
FIRMWARE="http://micropython.org/resources/firmware/esp32-idf3-20190529-v1.11.bin"

echo "Using $PORT"

if [ ! -f `basename $FIRMWARE` ]
then
    curl -O -J $FIRMWARE
fi

bin/esptool.py --port $PORT erase_flash && \
bin/esptool.py --chip esp32 --port $PORT --baud 115200 write_flash -z 0x1000 `basename $FIRMWARE`
