#!/bin/bash

cd bin && \
    mkdir -p envs && \
    cd envs && \
    python -m venv esptool && \
    source esptool/bin/activate && \
    pip install esptool && \
    deactivate && \
    cd .. && \
    ln -s $(pwd)/envs/esptool/bin/esptool.py 2>/dev/null
